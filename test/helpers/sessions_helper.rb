module SessionsHelper

  # Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
  end
  
   # Remembers a user in a persistent session.
  def remember(user)
  end
  
   def current_user
   end
  
  def logged_in?
    !current_user.nil?
  end
  
  def current_user?(user)
    user == current_user
  end
  
end