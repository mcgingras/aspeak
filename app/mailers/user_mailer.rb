class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Activate that Jawn"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Reset that Jawn"
  end
end